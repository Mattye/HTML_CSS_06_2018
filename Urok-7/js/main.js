document.addEventListener('DOMContentLoaded', function(){
	
	document.querySelectorAll('.share-project .normal-button')[0].onclick = () => {
		console.log('Share project button');
		document.querySelectorAll('.site-header, .color-selector-layout, .calendars-selector').forEach((currentTag) => 
		{
			currentTag.className += ' _blur';
		});

		document.querySelector('.popup-overlay').className += ' -flex';
		document.querySelector('.popup-window').className += ' -show';
	};
});